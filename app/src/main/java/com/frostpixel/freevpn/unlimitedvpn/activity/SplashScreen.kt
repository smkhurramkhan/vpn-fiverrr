package com.frostpixel.freevpn.unlimitedvpn.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.PowerManager
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.anchorfree.partner.api.auth.AuthMethod
import com.anchorfree.partner.api.response.User
import com.anchorfree.vpnsdk.callbacks.Callback
import com.anchorfree.vpnsdk.exceptions.VpnException
import com.frostpixel.freevpn.unlimitedvpn.BuildConfig
import com.frostpixel.freevpn.unlimitedvpn.MainApplication
import com.frostpixel.freevpn.unlimitedvpn.R
import com.frostpixel.freevpn.unlimitedvpn.databinding.ActivitySplashScreenBinding
import com.frostpixel.freevpn.unlimitedvpn.utils.BetterActivityResult
import com.frostpixel.freevpn.unlimitedvpn.utils.NetworkState

class SplashScreen : AppCompatActivity() {
    private val activityLauncher: BetterActivityResult<Intent, ActivityResult> =
        BetterActivityResult.registerActivityForResult(this)
    private lateinit var binding: ActivitySplashScreenBinding

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        MainApplication.loadInterstial()
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.textView4.text = getString(R.string.version) + BuildConfig.VERSION_NAME
        login()

    }


    override fun onResume() {
        super.onResume()
        startMain()
    }

    private fun login() {
        try {
            runOnUiThread {
                val authMethod: AuthMethod = AuthMethod.anonymous()
                MainApplication.unifiedSDK?.backend
                    ?.login(authMethod, object : Callback<User> {
                        override fun success(user: User) {}
                        override fun failure(e: VpnException) {
                            Toast.makeText(this@SplashScreen, e.message, Toast.LENGTH_SHORT).show()
                        }
                    })
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Toast.makeText(this@SplashScreen, ex.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun runProgress() {
        Log.d("khurram", "runing")
        Handler(mainLooper).postDelayed({
            startActivity(Intent(this@SplashScreen, MainActivity::class.java))
            finish()
            MainApplication.showInterstitial(this@SplashScreen)
        }, 4500)
    }

    private fun startMain() {
        if (!this.isFinishing) {
            if (NetworkState.isNetworkAvaliable(this)) {
                MainApplication.loadInterstial()
                runProgress()
            } else {
                val builder = AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.network_error))
                    .setMessage(getString(R.string.network_error_message))
                    .setNegativeButton(
                        getString(R.string.ok)
                    ) { dialog: DialogInterface, _: Int ->
                        dialog.cancel()
                        onBackPressed()
                    }
                val alert = builder.create()
                alert.show()
            }
        }
    }

    @SuppressLint("BatteryLife")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private fun Betteryoptpermission() {
        val pm: PowerManager =
            getApplicationContext().getSystemService(Context.POWER_SERVICE) as PowerManager
        val isIgnoringBatteryOptimizations: Boolean =
            pm.isIgnoringBatteryOptimizations(getPackageName())
        if (!isIgnoringBatteryOptimizations) {
            val intent = Intent()
            intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
            intent.setData(Uri.parse("package:" + getPackageName()))
            activityLauncher.launch(intent)
            activityLauncher.setOnActivityResult(BetterActivityResult.OnActivityResult<ActivityResult> { result: ActivityResult ->
                if (result.resultCode == Activity.RESULT_OK) {
                    val pm1: PowerManager = getSystemService(POWER_SERVICE) as PowerManager
                    val isIgnoringBatteryOptimizations1: Boolean =
                        pm1.isIgnoringBatteryOptimizations(getPackageName())
                    if (isIgnoringBatteryOptimizations1) {
                        startMain()
                    } else {
                        Betteryoptpermission()
                    }
                }
            })
        } else {
            startMain()
        }
    }
}
package com.frostpixel.freevpn.unlimitedvpn.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.anchorfree.reporting.TrackingConstants;
import com.anchorfree.sdk.SessionConfig;
import com.anchorfree.sdk.SessionInfo;
import com.anchorfree.sdk.UnifiedSDK;
import com.anchorfree.sdk.exceptions.PartnerApiException;
import com.anchorfree.sdk.rules.TrafficRule;
import com.anchorfree.vpnsdk.callbacks.Callback;
import com.anchorfree.vpnsdk.callbacks.CompletableCallback;
import com.anchorfree.vpnsdk.callbacks.VpnStateListener;
import com.anchorfree.vpnsdk.exceptions.NetworkRelatedException;
import com.anchorfree.vpnsdk.exceptions.VpnException;
import com.anchorfree.vpnsdk.exceptions.VpnPermissionDeniedException;
import com.anchorfree.vpnsdk.exceptions.VpnPermissionRevokedException;
import com.anchorfree.vpnsdk.transporthydra.HydraTransport;
import com.anchorfree.vpnsdk.transporthydra.HydraVpnTransportException;
import com.anchorfree.vpnsdk.vpnservice.VPNState;
import com.frostpixel.freevpn.unlimitedvpn.BuildConfig;
import com.frostpixel.freevpn.unlimitedvpn.MainApplication;
import com.frostpixel.freevpn.unlimitedvpn.R;
import com.frostpixel.freevpn.unlimitedvpn.utils.BetterActivityResult;
import com.frostpixel.freevpn.unlimitedvpn.utils.Constants;
import com.frostpixel.freevpn.unlimitedvpn.utils.SharedPrefs;
import com.google.android.material.navigation.NavigationView;
import com.northghost.caketube.CaketubeTransport;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pl.bclogic.pulsator4droid.library.PulsatorLayout;


public class MainActivity extends AppCompatActivity implements VpnStateListener,
        NavigationView.OnNavigationItemSelectedListener {

    private String selectedCountry;
    private String selectedCountryName;

    private CardView cv_country_selected, cv_activation;
    private ImageView ib_country_flag;
    private ImageView ib_dower;
    private TextView txt_country, txt_status;
    private RelativeLayout rl_activation;
    private PulsatorLayout pulsator;
    protected static final String TAG = MainActivity.class.getSimpleName();

    private boolean isActivateButton = true;

    protected final BetterActivityResult<Intent, ActivityResult> activityLauncher =
            BetterActivityResult.registerActivityForResult(this);


    private DrawerLayout drawer;
    final int SELECTED_COUNTRY = 1200;

    SharedPrefs prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        prefs = new SharedPrefs(this);

        init();
        onclickss();
        setupDrawer();

        Log.d("AdCount", "mainActivity count is " + prefs.getAdCount());


    }


    private void setupDrawer() {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                MainActivity.this, drawer, null, 0, 0);//R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(MainActivity.this);
    }

    private void onclickss() {
        cv_activation.setOnClickListener(view -> {
            MainApplication.showInterstitial(this);
            vpnActivation();
        });

        cv_country_selected.setOnClickListener(v -> {

            MainApplication.unifiedSDK.getBackend().isLoggedIn(new Callback<Boolean>() {
                @Override
                public void success(@NonNull Boolean isLoggedIn) {
                    if (isActivateButton) {
                        if (isLoggedIn) {

                            Intent i = new Intent(MainActivity.this, ChooseServerActivity.class);

                            activityLauncher.launch(i);
                            MainApplication.showInterstitial(MainActivity.this);

                            activityLauncher.setOnActivityResult(result -> {
                                if (result.getResultCode() == Activity.RESULT_OK) {
                                    String country = result.getData().getStringExtra(Constants.COUNTRYDATA).toLowerCase();

                                    System.out.println("mycountrydatais " + country);

                                    //last time selected servers
                                    Locale locale = new Locale("", country);
                                    if (country.equals("")) {

                                        txt_country.setText("Best Server");
                                        ib_country_flag.setImageResource(MainActivity.this.getResources().getIdentifier("drawable/earth", "drawable", MainActivity.this.getPackageName()));

                                    } else {

                                        txt_country.setText(locale.getDisplayCountry());
                                        ib_country_flag.setImageResource(MainActivity.this.getResources().getIdentifier("drawable/" + country, "drawable", MainActivity.this.getPackageName()));

                                    }
                                    selectedCountryName = locale.getDisplayCountry();
                                    selectedCountry = country;
                                    updateUI();

                                    UnifiedSDK.getVpnState(new Callback<VPNState>() {
                                        @Override
                                        public void success(@NonNull VPNState state) {
                                            if (state == VPNState.CONNECTED) {
                                                showMessage("Reconnecting to VPN with ");
                                                txt_status.setText("Reconnecting to VPN");
                                                MainApplication.unifiedSDK.getVPN().stop(TrackingConstants.GprReasons.M_UI, new CompletableCallback() {
                                                    @Override
                                                    public void complete() {
                                                        connectToVpn();
                                                    }

                                                    @Override
                                                    public void error(VpnException e) {
                                                        // In this case we try to reconnect
                                                        selectedCountry = "";
                                                        connectToVpn();
                                                        handleError(e);
                                                    }
                                                });
                                            }
                                        }

                                        @Override
                                        public void failure(@NonNull VpnException e) {
                                            handleError(e);
                                        }
                                    });
                                }
                            });


                        } else {
                            showMessage("Login please");
                        }
                    }
                }

                @Override
                public void failure(@NonNull VpnException e) {
                    handleError(e);
                }
            });
        });


        ib_dower.setOnClickListener(view -> drawer.openDrawer(GravityCompat.START));


    }

    private void vpnActivation() {

        UnifiedSDK.getVpnState(new Callback<VPNState>() {
            @Override
            public void success(@NonNull VPNState vpnState) {
                if (vpnState == VPNState.CONNECTED) {
                    disconnectToVpn();
                } else {
                    connectToVpn();
                }
            }

            @Override
            public void failure(@NonNull VpnException e) {
                Toast.makeText(MainActivity.this, "VPN Check failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void disconnectToVpn() {
        MainApplication.unifiedSDK.getVPN().stop(TrackingConstants.GprReasons.M_UI, new CompletableCallback() {
            @Override
            public void complete() {
                Toast.makeText(MainActivity.this, "VPN Disconnect", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void error(VpnException e) {
                handleError(e);
            }
        });
    }

    private void connectToVpn() {
        MainApplication.unifiedSDK.getBackend().isLoggedIn(new Callback<Boolean>() {
            @Override
            public void success(@NonNull Boolean isLoggedIn) {
                if (isLoggedIn) {
                    List<String> fallbackOrder = new ArrayList<>();
                    fallbackOrder.add(HydraTransport.TRANSPORT_ID);
                    fallbackOrder.add(CaketubeTransport.TRANSPORT_ID_TCP);
                    fallbackOrder.add(CaketubeTransport.TRANSPORT_ID_UDP);
                    List<String> bypassDomains = new LinkedList<>();
                    bypassDomains.add("*facebook.com");
                    bypassDomains.add("*wtfismyip.com");
                    UnifiedSDK.getInstance().getVPN().start(new SessionConfig.Builder()
                            .withReason(TrackingConstants.GprReasons.M_UI)
                            .withTransportFallback(fallbackOrder)
                            .withVirtualLocation(selectedCountry)
                            .withTransport(HydraTransport.TRANSPORT_ID)
                            .addDnsRule(TrafficRule.Builder.bypass().fromDomains(bypassDomains))
                            .build(), new CompletableCallback() {
                        @Override
                        public void complete() {
                            //btn_activation.setText("Deactive");
                            Toast.makeText(MainActivity.this, "VPN Connected",
                                    Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void error(@NonNull VpnException e) {
                            Toast.makeText(MainActivity.this, "Error Connecting ," +
                                    " Make Sure You have Selected A Country",
                                    Toast.LENGTH_LONG).show();

                        }
                    });
                } else {
                    Toast.makeText(MainActivity.this, "Restart VPN", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void failure(@NonNull VpnException e) {
                Log.d("khurram", e.getMessage());
                handleError(e);


            }
        });


    }

    public void handleError(Throwable e) {
        Log.w(TAG, e);
        if (e instanceof NetworkRelatedException) {
            showMessage("Check internet connection");
        } else if (e instanceof VpnException) {
            if (e instanceof VpnPermissionRevokedException) {
                showMessage("User revoked vpn permissions");
            } else if (e instanceof VpnPermissionDeniedException) {
                showMessage("User canceled to grant vpn permissions");
            } else if (e instanceof HydraVpnTransportException) {
                HydraVpnTransportException hydraVpnTransportException = (HydraVpnTransportException) e;
                if (hydraVpnTransportException.getCode() == HydraVpnTransportException.HYDRA_ERROR_BROKEN) {
                    showMessage("Connection with vpn server was lost");
                } else if (hydraVpnTransportException.getCode() == HydraVpnTransportException.HYDRA_DCN_BLOCKED_BW) {
                    showMessage("Client traffic exceeded");
                } else {
                    showMessage("Error in VPN transport");
                }
            } else {
                showMessage("Error in VPN Service");
            }
        } else if (e instanceof PartnerApiException) {
            switch (((PartnerApiException) e).getContent()) {
                case PartnerApiException.CODE_NOT_AUTHORIZED:
                    showMessage("User unauthorized");
                    break;
                case PartnerApiException.CODE_TRAFFIC_EXCEED:
                    showMessage("Server unavailable");
                    break;
                default:
                    showMessage("Other error. Check PartnerApiException constants");
                    break;
            }
        }
    }

    private void showMessage(String s) {
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    private void init() {
        LinearLayout banner_container = findViewById(R.id.banner_container);
        cv_country_selected = findViewById(R.id.cv_country_selected);
        ib_country_flag = findViewById(R.id.ib_country_flag);
        txt_country = findViewById(R.id.txt_country);
        txt_status = findViewById(R.id.txt_status);
        rl_activation = findViewById(R.id.rl_activation);
        cv_activation = findViewById(R.id.cv_activation);
        pulsator = findViewById(R.id.pulsator);
        ib_dower = findViewById(R.id.ib_dower);


        MainApplication.showBanner(banner_container);

    }

    @Override
    public void vpnStateChanged(@NonNull VPNState vpnState) {
        updateUI();
    }

    private void updateUI() {
        UnifiedSDK.getVpnState(new Callback<VPNState>() {
            @Override
            public void success(@NonNull VPNState vpnState) {

                switch (vpnState) {
                    case IDLE: {
                        pulsator.stop();
                        txt_status.setText("Not Connected");
                        rl_activation.setBackground(getResources().getDrawable(R.drawable.activation_disable_bg));
                        isActivateButton = true;
                        break;
                    }
                    case CONNECTED: {
                        pulsator.stop();
                        txt_status.setText("Connected");
                        rl_activation.setBackground(getResources().getDrawable(R.drawable.activation_bg));
                        isActivateButton = true;
                        break;
                    }
                    case CONNECTING_VPN:
                        txt_status.setText("Checking VPN");
                        rl_activation.setClickable(false);
                        rl_activation.setBackground(getResources().getDrawable(R.drawable.activation_bg));
                        isActivateButton = false;

                    case CONNECTING_CREDENTIALS:
                        txt_status.setText("Checking Credentials");
                        isActivateButton = false;

                    case CONNECTING_PERMISSIONS: {
                        txt_status.setText("Connecting VPN");
                        isActivateButton = false;

                        pulsator.start();
                        break;
                    }
                    case PAUSED: {
                        txt_status.setText("Paused");
                        rl_activation.setBackground(getResources().getDrawable(R.drawable.activation_bg));
                        isActivateButton = true;
                        break;
                    }
                }
            }

            @Override
            public void failure(@NonNull VpnException e) {
                pulsator.stop();
                rl_activation.setClickable(true);
                txt_status.setText("VPN can't connect, please try again..");
                rl_activation.setBackground(getResources().getDrawable(R.drawable.activation_disable_bg));
            }
        });
        MainApplication.unifiedSDK.getBackend().isLoggedIn(new Callback<Boolean>() {
            @Override
            public void success(@NonNull Boolean isLoggedIn) {

                //make connect button enabled
            }

            @Override
            public void failure(@NonNull VpnException e) {

            }
        });
        UnifiedSDK.getVpnState(new Callback<VPNState>() {
            @Override
            public void success(@NonNull VPNState state) {
                if (state == VPNState.CONNECTED) {
                    UnifiedSDK.getStatus(new Callback<SessionInfo>() {
                        @Override
                        public void success(@NonNull SessionInfo sessionInfo) {
                            runOnUiThread(() -> {
                                txt_country.setText(selectedCountryName != null ? selectedCountryName : "UNKNOWN");
                                ib_country_flag.setImageResource(MainActivity.this.getResources().getIdentifier("drawable/" + selectedCountry, "drawable", MainActivity.this.getPackageName()));
                            });

                        }

                        @Override
                        public void failure(@NonNull VpnException e) {
                            runOnUiThread(() -> {
                                txt_country.setText(selectedCountryName != null ? selectedCountryName : "UNKNOWN");
                                ib_country_flag.setImageResource(MainActivity.this.getResources().getIdentifier("drawable/" + selectedCountry, "drawable", MainActivity.this.getPackageName()));
                            });
                        }
                    });
                } else {
                    runOnUiThread(() -> {
                        txt_country.setText(selectedCountryName != null ? selectedCountryName : "UNKNOWN");
                        ib_country_flag.setImageResource(MainActivity.this.getResources().getIdentifier("drawable/" + selectedCountry, "drawable", MainActivity.this.getPackageName()));

                    });
                }
            }

            @Override
            public void failure(@NonNull VpnException e) {
                txt_country.setText("Select a country");
                ib_country_flag.setImageDrawable(getResources().getDrawable(R.drawable.select_flag_image));
            }
        });


    }

    @Override
    public void vpnError(@NonNull VpnException e) {
        updateUI();
        handleError(e);
        int j = 1;
        j++;
    }

    @Override
    protected void onStart() {
        super.onStart();


        SharedPrefs sharedPrefsFor = new SharedPrefs(this);
        Map<String, String> map = sharedPrefsFor.getPreference(SharedPrefs.PREFERENCE);
        if (map != null) {
            String country = map.get(SharedPrefs.PREFERENCE_selectedcountry) + "";

            Locale locale = new Locale("", country);

            selectedCountryName = locale.getDisplayCountry();
            selectedCountry = locale.getCountry().toLowerCase();
        }
        UnifiedSDK.addVpnStateListener(this);

    }


    @Override
    protected void onStop() {
        super.onStop();
        UnifiedSDK.removeVpnStateListener(this);
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_helpus) {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"joelcsosauy@gmail.com"});
            intent.putExtra(Intent.EXTRA_SUBJECT, "Improve Comments");
            intent.putExtra(Intent.EXTRA_TEXT, "message body");

            try {
                startActivity(Intent.createChooser(intent, "send mail"));
            } catch (ActivityNotFoundException ex) {
                Toast.makeText(this, "No mail app found!!!", Toast.LENGTH_SHORT).show();
            } catch (Exception ex) {
                Toast.makeText(this, "Unexpected Error!!!", Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_rate) {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id="
                                + appPackageName)));
            }
        } else if (id == R.id.nav_share) {
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "share app");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "I'm using this Free VPN App, it's provide all servers free https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        } else if (id == R.id.nav_policy) {
            try {
                Uri uri = Uri.parse(getResources().getString(R.string.privacy_policy_link)); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this, "No application found to handle this", Toast.LENGTH_LONG).show();
            }

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}

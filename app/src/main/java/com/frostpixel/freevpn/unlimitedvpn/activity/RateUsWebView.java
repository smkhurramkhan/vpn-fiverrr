package com.frostpixel.freevpn.unlimitedvpn.activity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import com.frostpixel.freevpn.unlimitedvpn.R;


public class RateUsWebView extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_us_web_view);

        Intent i = getIntent();
        String url = i.getStringExtra("link");
        WebView webView = findViewById(R.id.webview_view);
        webView.loadUrl(url);
    }


}
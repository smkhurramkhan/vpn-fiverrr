package com.frostpixel.freevpn.unlimitedvpn.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.anchorfree.partner.api.data.Country;
import com.anchorfree.partner.api.response.AvailableCountries;
import com.anchorfree.sdk.UnifiedSDK;
import com.anchorfree.vpnsdk.callbacks.Callback;
import com.anchorfree.vpnsdk.exceptions.VpnException;
import com.frostpixel.freevpn.unlimitedvpn.adapter.RegionListAdapter;
import com.frostpixel.freevpn.unlimitedvpn.databinding.ActivityChooseServerBinding;
import com.frostpixel.freevpn.unlimitedvpn.utils.Constants;
import com.frostpixel.freevpn.unlimitedvpn.utils.SharedPrefs;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class ChooseServerActivity extends AppCompatActivity {
    ActivityChooseServerBinding binding;
    private RegionListAdapter regionAdapter;
    private RegionChooserInterface regionChooserInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChooseServerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        setSupportActionBar(binding.toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        regionChooserInterface = item -> {

            System.out.println("mycountrydatais " + item.getCountry());

            Map<String, String> map = new HashMap<>();

            map.put(SharedPrefs.PREFERENCE_selectedcountry, item.getCountry());


            SharedPrefs sharedPrefsFor = new SharedPrefs(ChooseServerActivity.this);

            sharedPrefsFor.setPreference(map);

            Intent intent = new Intent();
            intent.putExtra(Constants.COUNTRYDATA, item.getCountry());
            setResult(RESULT_OK, intent);
            finish();

        };

        binding.regionsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        regionAdapter = new RegionListAdapter(item -> regionChooserInterface.onRegionSelected(item),
                ChooseServerActivity.this);
        binding.regionsRecyclerView.setAdapter(regionAdapter);

        binding.swipelayout.setOnRefreshListener(() -> {

            loadServers();

            Toast.makeText(ChooseServerActivity.this, "Refreshed", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> binding.swipelayout.setRefreshing(false), 2000);
        });


        loadServers();


    }


    private void loadServers() {
        showProgress();
        UnifiedSDK.getInstance().getBackend().countries(new Callback<AvailableCountries>() {
            @Override
            public void success(@NonNull final AvailableCountries countries) {
                hideProress();
                regionAdapter.setRegions(countries.getCountries());
                regionAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(@NonNull VpnException e) {
                hideProress();
            }
        });
    }

    private void showProgress() {
        binding.regionsProgress.setVisibility(View.VISIBLE);
        binding.regionsRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void hideProress() {
        binding.regionsProgress.setVisibility(View.GONE);
        binding.regionsRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public interface RegionChooserInterface {
        void onRegionSelected(Country item);
    }
}

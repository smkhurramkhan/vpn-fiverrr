package com.frostpixel.freevpn.unlimitedvpn

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.app.Application.ActivityLifecycleCallbacks
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.webkit.WebView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.work.Configuration
import androidx.work.WorkManager
import com.anchorfree.partner.api.ClientInfo
import com.anchorfree.sdk.HydraTransportConfig
import com.anchorfree.sdk.NotificationConfig
import com.anchorfree.sdk.TransportConfig
import com.anchorfree.sdk.UnifiedSDK
import com.anchorfree.vpnsdk.callbacks.CompletableCallback
import com.facebook.ads.AudienceNetworkAds
import com.frostpixel.freevpn.unlimitedvpn.activity.AdsWaitActivity
import com.frostpixel.freevpn.unlimitedvpn.activity.SplashScreen
import com.frostpixel.freevpn.unlimitedvpn.ads.AdMobManager
import com.frostpixel.freevpn.unlimitedvpn.ads.AdsFacebookManger
import com.frostpixel.freevpn.unlimitedvpn.ads.ConnectionDetector
import com.frostpixel.freevpn.unlimitedvpn.utils.SharedPrefs
import com.google.android.gms.ads.*
import com.google.android.gms.ads.appopen.AppOpenAd
import com.google.android.gms.ads.appopen.AppOpenAd.AppOpenAdLoadCallback
import com.northghost.caketube.OpenVpnTransportConfig
import com.onesignal.OneSignal
import java.util.*


class MainApplication : Application(), Configuration.Provider, ActivityLifecycleCallbacks,
    LifecycleObserver {
    private var appOpenAdManager: AppOpenAdManager? = null
    private var currentActivity: Activity? = null


    private var prefs: SharedPrefs? = null
    override fun onCreate() {
        super.onCreate()
        try {
            initHydraSdk()
            instance = this
            prefs = SharedPrefs(this)


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                val process = getProcessName()
                if (packageName != process) WebView.setDataDirectorySuffix(process)
            }

            MobileAds.initialize(this)


            ProcessLifecycleOwner.get().lifecycle.addObserver(this)
            appOpenAdManager = AppOpenAdManager(this)
            OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)

            // OneSignal Initialization
            OneSignal.initWithContext(this)
            OneSignal.setAppId(getString(R.string.onsignalappid))
            AudienceNetworkAds.initialize(this)
            adsFacebookManger = AdsFacebookManger(this)
            adMobManager = AdMobManager(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    /**
     * LifecycleObserver method that shows the app open ad when the app moves to foreground.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        if (currentActivity != null && currentActivity is SplashScreen) {
            appOpenAdManager?.loadAd(currentActivity!!)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    protected fun onMoveToForeground() {
        // Show the ad (if available) when the app moves to foreground.
        if (currentActivity != null && currentActivity is SplashScreen) {
            Log.d("khurram", "splash")
        } else {
            appOpenAdManager!!.showAdIfAvailable(currentActivity!!)
        }
    }

    /**
     * ActivityLifecycleCallback methods.
     */
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {}
    override fun onActivityStarted(activity: Activity) {
        // An ad activity is started when an ad is showing, which could be AdActivity class from Google
        // SDK or another activity class implemented by a third party mediation partner. Updating the
        // currentActivity only when an ad is not showing will ensure it is not an ad activity, but the
        // one that shows the ad.
        if (!appOpenAdManager!!.isShowingAd) {
            currentActivity = activity
            Log.d(
                "Appopen", "onActivityStarted current activity is"
                        + currentActivity!!.localClassName
            )
        }
    }

    override fun onActivityResumed(activity: Activity) {
        currentActivity = activity
        Log.d(
            "Appopen", "onActivityResumed current activity is"
                    + currentActivity!!.localClassName
        )
    }

    override fun onActivityPaused(activity: Activity) {}
    override fun onActivityStopped(activity: Activity) {}
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}
    override fun onActivityDestroyed(activity: Activity) {}

    private fun initHydraSdk() {
        createNotificationChannel()
        val clientInfo = ClientInfo.newBuilder()
            .baseUrl(BuildConfig.BASE_HOST)
            .carrierId(BuildConfig.BASE_CARRIER_ID)
            .build()
        unifiedSDK = UnifiedSDK.getInstance(clientInfo)
        val transportConfigList: MutableList<TransportConfig> = ArrayList()
        transportConfigList.add(HydraTransportConfig.create())
        transportConfigList.add(OpenVpnTransportConfig.tcp())
        transportConfigList.add(OpenVpnTransportConfig.udp())
        UnifiedSDK.update(transportConfigList, CompletableCallback.EMPTY)
        val notificationConfig = NotificationConfig.newBuilder()
            .title(resources.getString(R.string.app_name))
            .channelId(CHANNEL_ID)
            .build()
        UnifiedSDK.update(notificationConfig)
        UnifiedSDK.setLoggingLevel(Log.VERBOSE)
    }

    fun getPrefs(): SharedPreferences {
        return getSharedPreferences(BuildConfig.SHARED_PREFS, MODE_PRIVATE)
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "Cloud Vpn"
            val description = "Cloud Vpn notification"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(
                NotificationManager::class.java
            )
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun getWorkManagerConfiguration(): Configuration {
        return Configuration.Builder()
            .build()
    }

    /**
     * Interface definition for a callback to be invoked when an app open ad is complete
     * (i.e. dismissed or fails to show).
     */
    interface OnShowAdCompleteListener {
        fun onShowAdComplete()
    }

    /**
     * Inner class that loads and shows app open ads.
     */

    public class AppOpenAdManager(mainApplication: MainApplication) {

        private var isLoadingAd = false
        var isShowingAd = false

        /**
         * Load an ad.
         *
         * @param context the context of the activity that loads the ad
         */


        public fun loadAd(context: Context) {
            // Do not load ad if there is an unused ad or one is already loading.
            if (isLoadingAd || isAdAvailable) {
                return
            }
            isLoadingAd = true
            val request = AdRequest.Builder().build()
            AppOpenAd.load(
                context,
                context.getString(R.string.AdmobAppopenads),
                request,
                AppOpenAd.APP_OPEN_AD_ORIENTATION_PORTRAIT,
                object : AppOpenAdLoadCallback() {
                    /**
                     * Called when an app open ad has loaded.
                     *
                     * @param ad the loaded app open ad.
                     */
                    override fun onAdLoaded(ad: AppOpenAd) {
                        appOpenAd = ad
                        isLoadingAd = false
                        Log.d(LOG_TAG, "onAdLoaded.")
                    }

                    /**
                     * Called when an app open ad has failed to load.
                     *
                     * @param loadAdError the error.
                     */
                    override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                        isLoadingAd = false
                        Log.d(LOG_TAG, "onAdFailedToLoad: " + loadAdError.message)
                    }
                })
        }

        /**
         * Check if ad exists and can be shown.
         */
        private val isAdAvailable: Boolean
            get() =
                appOpenAd != null

        /**
         * Show the ad if one isn't already showing.
         *
         * @param activity the activity that shows the app open ad
         */
        fun showAdIfAvailable(activity: Activity) {
            if (activity is SplashScreen) {
                Log.d("Appopen", "splash")
            } else {
                showAdIfAvailable(
                    activity,
                    object : OnShowAdCompleteListener {
                        override fun onShowAdComplete() {
                            Log.d("Appopen", "onShowAdComplete list $activity")
                        }
                    })
            }
        }

        /**
         * Show the ad if one isn't already showing.
         *
         * @param activity                 the activity that shows the app open ad
         * @param onShowAdCompleteListener the listener to be notified when an app open ad is complete
         */
        private fun showAdIfAvailable(
            activity: Activity,
            onShowAdCompleteListener: OnShowAdCompleteListener
        ) {
            // If the app open ad is already showing, do not show the ad again.
            if (isShowingAd) {
                Log.d(LOG_TAG, "The app open ad is already showing.")
                return
            }

            // If the app open ad is not available yet, invoke the callback then load the ad.
            if (!isAdAvailable) {
                Log.d(LOG_TAG, "The app open ad is not ready yet.")
                onShowAdCompleteListener.onShowAdComplete()
                loadAd(activity)
                return
            }

            Log.d(LOG_TAG, "Will show ad.")



            appOpenAd!!.fullScreenContentCallback = object : FullScreenContentCallback() {
                /** Called when full screen content is dismissed.  */
                override fun onAdDismissedFullScreenContent() {
                    // Set the reference to null so isAdAvailable() returns false.
                    appOpenAd = null
                    isShowingAd = false

                    callbackExit.hideActivity()
                    Log.d(LOG_TAG, "onAdDismissedFullScreenContent.")

                    onShowAdCompleteListener.onShowAdComplete()
                    loadAd(activity)
                }

                /** Called when fullscreen content failed to show.  */
                override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                    appOpenAd = null
                    isShowingAd = false
                    Log.d(LOG_TAG, "onAdFailedToShowFullScreenContent: " + adError.message)
                    onShowAdCompleteListener.onShowAdComplete()
                    loadAd(activity)
                }

                /** Called when fullscreen content is shown.  */
                override fun onAdShowedFullScreenContent() {
                    Log.d(LOG_TAG, "onAdShowedFullScreenContent.")
                }
            }
            isShowingAd = true
            if (activity.localClassName != "SplashScreen") {
                activity.startActivity(Intent(activity, AdsWaitActivity::class.java))
                Log.d("khurram", "onStart")
            }
        }

        companion object {
            private const val LOG_TAG = "AppOpenAdManager"
            var appOpenAd: AppOpenAd? = null

        }

        /**
         * Constructor.
         */
        init {
            mainApplication.registerActivityLifecycleCallbacks(mainApplication)
        }
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        var instance: MainApplication? = null
            private set
        private const val CHANNEL_ID = "cloud_vpn"
        public lateinit var callbackExit: CallbackExit

        @JvmField
        var unifiedSDK: UnifiedSDK? = null
        var adMobManager: AdMobManager? = null
        var adsFacebookManger: AdsFacebookManger? = null

        @JvmStatic
        fun loadInterstial() {
            adMobManager!!.loadAdMobInterstitialAd()
            adsFacebookManger!!.loadFbInterstitial()
        }


        @JvmStatic
        fun showInterstitial(activity: Activity?) {
            if (ConnectionDetector.isNotConnectedToInternet) return
            if (adMobManager!!.interstitialAd != null) {
                adMobManager!!.interstitialAd?.show(activity!!)
                adMobManager!!.loadAdMobInterstitialAd()
            } else if (adsFacebookManger!!.fbInterstitialAd != null
                && adsFacebookManger!!.fbInterstitialAd!!.isAdLoaded
            ) {
                adsFacebookManger!!.fbInterstitialAd?.show()
                adMobManager!!.loadAdMobInterstitialAd()
            }
        }

        @JvmStatic
        fun showBanner(bannerAdContainer: ViewGroup) {
            if (ConnectionDetector.isNotConnectedToInternet) return
            adMobManager!!.adMobAdaptiveBanner(bannerAdContainer)
        }


        val workManager: WorkManager
            get() = WorkManager.getInstance(instance!!)

        fun setInter(callbackExit: CallbackExit) {
            this.callbackExit = callbackExit
        }
    }

    interface CallbackExit {
        fun hideActivity() {

        }
    }
}
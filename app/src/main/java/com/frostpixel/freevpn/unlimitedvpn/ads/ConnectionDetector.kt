package com.frostpixel.freevpn.unlimitedvpn.ads

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.frostpixel.freevpn.unlimitedvpn.MainApplication.Companion.instance

object ConnectionDetector {
    val isNotConnectedToInternet: Boolean
        get() {
            var connectivity: ConnectivityManager? = null
            try {
                connectivity =
                    instance!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (connectivity != null) {
                val info = connectivity.allNetworkInfo
                for (networkInfo in info) if (networkInfo.state == NetworkInfo.State.CONNECTED) {
                    return false
                }
            }
            return true
        }
}
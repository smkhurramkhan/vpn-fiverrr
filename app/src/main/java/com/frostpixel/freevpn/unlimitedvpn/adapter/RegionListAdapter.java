package com.frostpixel.freevpn.unlimitedvpn.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anchorfree.partner.api.data.Country;
import com.frostpixel.freevpn.unlimitedvpn.MainApplication;
import com.frostpixel.freevpn.unlimitedvpn.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class RegionListAdapter extends RecyclerView.Adapter<RegionListAdapter.ViewHolder> {

    private final String nn;
    public Context context;
    private List<Country> regions;
    private final RegionListAdapterInterface listAdapterInterface;


    public RegionListAdapter(RegionListAdapterInterface listAdapterInterface, Activity cntec) {
        this.listAdapterInterface = listAdapterInterface;
        this.context = cntec;

        SharedPreferences prefs = cntec.getSharedPreferences("whatsapp_pref",
                Context.MODE_PRIVATE);
        nn = prefs.getString("inappads", "nnn");//"No name defined" is the default value.

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.server_list_free, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        final Country data = this.regions.get(position);
        Locale locale = new Locale("", data.getCountry());

/*        if (position == 0) {

            holder.flag.setImageResource(R.drawable.earth);
            holder.app_name.setText("Best Server");
            holder.app_name.setTextColor(context.getResources().getColor(R.color.white));
            holder.limit.setVisibility(View.GONE);

        } else {*/

            Resources resources = context.getResources();
            String sb = "drawable/" + data.getCountry().toLowerCase();
            holder.flag.setImageResource(resources.getIdentifier(sb, null, context.getPackageName()));
            holder.app_name.setText(locale.getDisplayCountry());
            holder.limit.setVisibility(View.VISIBLE);


/*
        }*/

        holder.itemView.setOnClickListener(view -> {
            MainApplication.showInterstitial((Activity) context);
            listAdapterInterface.onCountrySelected(regions.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return regions != null ? regions.size() : 0;
    }

    public void setRegions(List<Country> list) {
        regions = new ArrayList<>();
        regions.addAll(list);
        notifyDataSetChanged();
    }

    public interface RegionListAdapterInterface {
        void onCountrySelected(Country item);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView app_name;
        ImageView flag;
        ImageView limit;

        ViewHolder(View v) {
            super(v);
            this.app_name = itemView.findViewById(R.id.region_title);
            this.limit = itemView.findViewById(R.id.region_limit);
            this.flag = itemView.findViewById(R.id.country_flag);

        }
    }
}

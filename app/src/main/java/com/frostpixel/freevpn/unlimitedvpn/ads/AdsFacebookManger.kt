package com.frostpixel.freevpn.unlimitedvpn.ads

import android.content.Context
import android.util.DisplayMetrics
import android.util.Log
import android.view.ViewGroup
import android.view.WindowManager
import com.facebook.ads.*
import com.frostpixel.freevpn.unlimitedvpn.MainApplication
import com.frostpixel.freevpn.unlimitedvpn.R

class AdsFacebookManger
/**
 * Constructor
 */(private var myApplication: MainApplication) {
    var fbInterstitialAd: InterstitialAd? = null
        private set

    //facebook banner ads
    private var bannerAdView: AdView? = null

    /**
     * Load Facebook Interstitial
     */
    fun loadFbInterstitial() {
        if (fbInterstitialAd != null) {
            fbInterstitialAd!!.destroy()
            fbInterstitialAd = null
        }
        val interstitialAdExtendedListener: InterstitialAdExtendedListener =
            object : InterstitialAdExtendedListener {
                override fun onInterstitialActivityDestroyed() {}
                override fun onInterstitialDisplayed(ad: Ad) {}
                override fun onInterstitialDismissed(ad: Ad) {
                    loadFbInterstitial()
                }

                override fun onError(ad: Ad, adError: AdError) {
                    Log.d("loadFbInterstitial", "loadFbInterstitial: " + adError.errorMessage)
                }

                override fun onAdLoaded(ad: Ad) {}
                override fun onAdClicked(ad: Ad) {}
                override fun onLoggingImpression(ad: Ad) {}
                override fun onRewardedAdCompleted() {}
                override fun onRewardedAdServerSucceeded() {}
                override fun onRewardedAdServerFailed() {}
            }
        fbInterstitialAd = InterstitialAd(
            myApplication,
            myApplication.resources.getString(R.string.fbAdmobInterstitial)
        )
        // Load a new interstitial.
        val loadAdConfig = fbInterstitialAd!!
            .buildLoadAdConfig() // Set a listener to get notified on changes
            // or when the user interact with the ad.
            .withAdListener(interstitialAdExtendedListener) // .withCacheFlags(EnumSet.of(CacheFlag.VIDEO))
            //.withRewardData(new RewardData("YOUR_USER_ID", "YOUR_REWARD", 10))
            .build()
        fbInterstitialAd!!.loadAd(loadAdConfig)
    }

    /**
     * Request to Facebook Banner Ad
     */
    private val bannerHeightInPixel: Int
        get() {
            val display =
                (myApplication.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
            val outMetrics = DisplayMetrics()
            display.getMetrics(outMetrics)
            val scale = outMetrics.density
            return (50 * scale + 0.5f).toInt()
        }

    fun fbBannerAdView(adContainerView: ViewGroup) {
        adContainerView.layoutParams.height = bannerHeightInPixel
        val adListener: AdListener = object : AdListener {
            override fun onError(ad: Ad, adError: AdError) {
                Log.d("LOGCAT", "fbBannerAdView: " + adError.errorMessage)
            }

            override fun onAdLoaded(ad: Ad) {
                if (bannerAdView!!.parent != null) {
                    (bannerAdView!!.parent as ViewGroup).removeView(bannerAdView)
                }
                adContainerView.addView(bannerAdView)
            }

            override fun onAdClicked(ad: Ad) {}
            override fun onLoggingImpression(ad: Ad) {}
        }
        // app settings). Use different ID for each ad placement in your app.
        bannerAdView = AdView(
            myApplication,
            myApplication.resources.getString(R.string.fbAdmobBanner),
            AdSize.BANNER_HEIGHT_50
        )

        // Initiate a request to load an ad.
        bannerAdView!!.loadAd(bannerAdView!!.buildLoadAdConfig().withAdListener(adListener).build())
    }
}
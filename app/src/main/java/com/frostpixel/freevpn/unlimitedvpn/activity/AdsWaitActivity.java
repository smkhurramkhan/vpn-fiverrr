package com.frostpixel.freevpn.unlimitedvpn.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.frostpixel.freevpn.unlimitedvpn.MainApplication;
import com.frostpixel.freevpn.unlimitedvpn.R;

public class AdsWaitActivity extends AppCompatActivity implements MainApplication.CallbackExit {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads_wait);


        MainApplication.Companion.setInter(this);

        if (MainApplication.AppOpenAdManager.Companion.getAppOpenAd() != null)
            MainApplication.AppOpenAdManager.Companion.getAppOpenAd().show(this);

    }


    @Override
    public void hideActivity() {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.nothing, R.anim.bottom_down);
    }
}
package com.frostpixel.freevpn.unlimitedvpn.ads

import android.app.Application
import android.content.Context
import android.content.res.Resources.NotFoundException
import android.net.ConnectivityManager
import android.util.DisplayMetrics
import android.util.Log
import android.view.ViewGroup
import android.view.WindowManager
import com.frostpixel.freevpn.unlimitedvpn.MainApplication
import com.frostpixel.freevpn.unlimitedvpn.R
import com.google.android.gms.ads.*
import com.google.android.gms.ads.initialization.InitializationStatus
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback

class AdMobManager(private val myApplication: MainApplication?) {
    var interstitialAd: InterstitialAd? = null
        private set
    private var bannerAdView: AdView? = null
    private val adsFacebookManger: AdsFacebookManger = AdsFacebookManger(myApplication!!)

    private fun checkConnection(): Boolean {
        val connMgr =
            myApplication!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connMgr.activeNetworkInfo
        if (activeNetworkInfo != null) {
            return if (activeNetworkInfo.type == ConnectivityManager.TYPE_WIFI) {
                true
            } else activeNetworkInfo.type == ConnectivityManager.TYPE_MOBILE
        }
        return false
    }

    //  Load admob interstitial
    fun loadAdMobInterstitialAd() {
        if (!checkConnection()) return
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            myApplication!!, myApplication.resources.getString(R.string.AdmobInterstitial),
            adRequest, object : InterstitialAdLoadCallback() {
                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    this@AdMobManager.interstitialAd = interstitialAd
                    interstitialAd.fullScreenContentCallback = fullScreenContentCallback
                }

                override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                    Log.d("Aderror", loadAdError.message)
                    interstitialAd = null
                }
            })
    }

    var fullScreenContentCallback: FullScreenContentCallback =
        object : FullScreenContentCallback() {
            override fun onAdDismissedFullScreenContent() {
                Log.d("Aderror", "The ad was dismissed.")
                loadAdMobInterstitialAd()
            }

            override fun onAdFailedToShowFullScreenContent(adError: AdError) {
                Log.d("Aderror", "The ad failed to show.")
            }

            override fun onAdShowedFullScreenContent() {
                interstitialAd = null
                Log.d("Aderror", "The ad was shown.")
            }
        }

    // Step 2 - Determine the screen width (less decorations) to use for the ad width.
    private val adSize: AdSize
        get() {
            val display =
                (myApplication!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
            val outMetrics = DisplayMetrics()
            display.getMetrics(outMetrics)
            val widthPixels = outMetrics.widthPixels.toFloat()
            val density = outMetrics.density
            val adWidth = (widthPixels / density).toInt()

            return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(
                myApplication, adWidth
            )
        }

    //admob adaptive banner
    fun adMobAdaptiveBanner(adContainerView: ViewGroup) {
        try {
            if (myApplication != null) {
                bannerAdView = AdView(myApplication)
                bannerAdView!!.adUnitId = myApplication.resources.getString(R.string.AdmobBanner)
                val adSize = adSize
                adContainerView.layoutParams.height = getPixelFromDp(
                    myApplication, 100
                )
                bannerAdView!!.adSize = adSize
                bannerAdView!!.adListener = object : AdListener() {
                    override fun onAdLoaded() {
                        if (bannerAdView!!.parent != null) {
                            (bannerAdView!!.parent as ViewGroup).removeView(bannerAdView) // <- fix
                        }
                        adContainerView.addView(bannerAdView)
                    }

                    override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                        super.onAdFailedToLoad(loadAdError)
                        adsFacebookManger.fbBannerAdView(adContainerView)
                        Log.d("Aderror", "banner error code is " + loadAdError.message)
                    }

                    override fun onAdClosed() {}
                }
                val adRequest = AdRequest.Builder().build()
                // Start loading the ad in the background.
                bannerAdView!!.loadAd(adRequest)
            }
        } catch (e: NotFoundException) {
            e.printStackTrace()
        }
    }

    companion object {
        fun getPixelFromDp(application: Application, dp: Int): Int {
            val display =
                (application.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
            val outMetrics = DisplayMetrics()
            display.getMetrics(outMetrics)
            val scale = outMetrics.density
            return (dp * scale + 0.5f).toInt()
        }
    }

    /**
     * Constructor
     */
/*    init {
        MobileAds.initialize(myApplication!!) { initializationStatus: InitializationStatus? -> }
    }*/
}